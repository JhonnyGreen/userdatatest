//
//  UserEntity.swift
//  UserDataTest
//
//  Created by Jhonny Green on 04.05.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import Foundation
import CoreData

class UserEntity: NSManagedObject {
    
    class func findOrCreate(_ user: Results, context: NSManagedObjectContext) throws -> UserEntity {
        
        let request: NSFetchRequest<UserEntity> = UserEntity.fetchRequest()
        request.predicate = NSPredicate(format: "firstName == %d", user.name.first)
        
        do {
            let fetchResult = try context.fetch(request)
            if fetchResult.count > 0 {
                assert(fetchResult.count == 1, "Duplicate has been found in data base!")
                return fetchResult[0]
            }
        } catch {
            throw error
        }
        let userEntity = UserEntity(context: context)
        userEntity.firstName = user.name.first
        userEntity.lastName = user.name.last
        userEntity.title = user.name.title
        userEntity.email = user.email
        userEntity.phone = user.phone
        userEntity.image = user.picture.thumbnail
        
        return userEntity
    }
    
    class func all(_ context: NSManagedObjectContext) throws -> [UserEntity] {
        let request: NSFetchRequest<UserEntity> = UserEntity.fetchRequest()
        do {
            return  try context.fetch(request)

        } catch {
            throw error
        }
    }
}
