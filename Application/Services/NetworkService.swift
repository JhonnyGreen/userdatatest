//
//  NetworkService.swift
//  UserDataTest
//
//  Created by Jhonny Green on 29.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import Foundation
import Alamofire


class NetworkService {
    
    static func fetchUsers(page: Int, results: Int, seed: String, completion: @escaping (_ usersFromIntermet: [Results]) -> ()) {

        let urlString = "https://randomuser.me/api/?page=\(page)&results=\(results)&seed=\(seed)"
        
        guard let url = URL(string: urlString) else { return }
        print(url)
        
        
        
        AF.request(url, method: .get).validate().responseData { response in
            
            switch response.result {
            case .success(let data):
                let decoder = JSONDecoder()
                if let usersResponse = try? decoder.decode(Empty.self, from: data) {
                    
                    let users = usersResponse.results
                    completion(users)
                    
                } else {
                    print("Invalid response data in users")
                }
                
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
