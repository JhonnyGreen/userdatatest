//
//  UsersTableViewController.swift
//  UserDataTest
//
//  Created by Jhonny Green on 29.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class UsersTableViewController: UIViewController {
    
    
    var usersArray = [Results]()
    
    
    var totalEntries = 100
    
    var page = 1
    var results = 10
    var seed = "abc"
    
    
    @IBOutlet weak var usersTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usersTableView.dataSource = self
        usersTableView.delegate = self
        
        fetchUsersAF()
    }
    
    
    func fetchUsersAF() {
        NetworkService.fetchUsers(page: page, results: results, seed: seed) { users in
            if users.isEmpty {
                print("Users array is empty")
            } else {
                self.usersArray = users
                DispatchQueue.main.async {
                    self.usersTableView.reloadData()
                }
            }
        }
    }
    
    
    func configureCell(cell: UsersTableViewCell, for indexPath: IndexPath) {
        let user = usersArray[indexPath.row]
        cell.phoneNumberLabel.text = user.phone
        cell.userNameLabel.text = "\(user.name.title)" + " " + "\(user.name.first)" + " " + "\(user.name.last)"
        
        let path = usersArray[indexPath.row].picture.thumbnail
        guard let url = URL(string: path) else { return }
        AF.request(url).responseData { [cell] result in
            guard let imageData = result.data else {
                print("error connected with image fetch")
                return
            }
            let image = UIImage(data: imageData)
            let size = CGSize(width: 75.0, height: 75.0)
            let scaledImage = image?.af.imageScaled(to: size)
            let roundedImage = scaledImage?.af.imageRounded(withCornerRadius: 10.0)
            DispatchQueue.main.async {
                cell.userImageView.image = roundedImage
                self.usersTableView.reloadData()
            }
        }
    }
}
extension UsersTableViewController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return usersArray.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UsersCell", for: indexPath) as! UsersTableViewCell
        
        configureCell(cell: cell, for: indexPath)
        
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "userDetailsSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "userDetailsSegue" {
            guard let indexPath = usersTableView.indexPathForSelectedRow else { return }
            let user = usersArray[indexPath.row]
            let userDetailsVC = segue.destination as! UserDetailsViewController
            userDetailsVC.usersDetails = user
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = usersArray.count - 1
        if indexPath.row == lastItem {
            // load more data
        }
    }
}

