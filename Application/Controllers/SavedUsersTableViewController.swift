//
//  SavedUsersTableViewController.swift
//  UserDataTest
//
//  Created by Jhonny Green on 29.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit

class SavedUsersTableViewController: UIViewController {

    @IBOutlet weak var savedUsersTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
extension SavedUsersTableViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: - Table view data source

     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }

    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SavedUsersCell", for: indexPath) as! SavedUsersTableViewCell

        // Configure the cell...

        return cell
    }
    

}
