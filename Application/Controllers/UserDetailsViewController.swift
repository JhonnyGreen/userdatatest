//
//  UserDetailsViewController.swift
//  UserDataTest
//
//  Created by Jhonny Green on 30.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit
import AlamofireImage
import Alamofire
import PhoneNumberKit

class UserDetailsViewController: UITableViewController{
    
    var usersDetails: Results?
    
    
    @IBOutlet var userDetailsTableView: UITableView!
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: PhoneNumberTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        userDetailsTableView.delegate = self
        userDetailsTableView.dataSource = self
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        
        setupEditScreen()
        
        tableView.tableFooterView = UIView()
        
    }
    
    
    // MARK: Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            
            let cameraIcon = #imageLiteral(resourceName: "camera")
            let photoIcon = #imageLiteral(resourceName: "photo")
            
            let actionSheet = UIAlertController(title: nil,
                                                message: nil,
                                                preferredStyle: .actionSheet)
            let camera = UIAlertAction(title: "Camera", style: .default) { _ in
                self.chooseImagePicker(source: .camera)
            }
            camera.setValue(cameraIcon, forKey: "image")
            camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            let photo = UIAlertAction(title: "Photo", style: .default) { _ in
                self.chooseImagePicker(source: .photoLibrary)
            }
            photo.setValue(photoIcon, forKey: "image")
            photo.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel)
            actionSheet.addAction(camera)
            actionSheet.addAction(photo)
            actionSheet.addAction(cancel)
            
            present(actionSheet, animated: true)
        } else {
            view.endEditing(true)
        }
    }

    
    func isValidEmail(testStr: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: testStr)
    }


    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let currentFirstText = firstNameTextField.text ?? ""

        guard let stringRange = Range(range, in: currentFirstText) else { return false }

        let updatedText = currentFirstText.replacingCharacters(in: stringRange, with: string)

        return updatedText.count <= 30
    }
    
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(action) in alert.dismiss(animated: true, completion: nil)}))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    @IBAction func saveButtonAction(_ sender: UIBarButtonItem) {
        let email = isValidEmail(testStr: emailTextField.text ?? "")
        
        if email == false {
            showAlert(title: "Error!", message: "This is not valid email. Please try again.")
            emailTextField.text = ""
        }
    }
    private func setupEditScreen() {
        if usersDetails != nil {
            
            firstNameTextField.text = usersDetails?.name.first
            lastNameTextField.text = usersDetails?.name.last
            emailTextField.text = usersDetails?.email
            phoneTextField.text = usersDetails?.phone
            let path = usersDetails?.picture.medium
            guard let url = URL(string: path!) else { return }
            AF.request(url).responseData { result in
                guard let imageData = result.data else {
                    print("error connected with image fetch")
                    return
                }
                let image = UIImage(data: imageData)
                let size = CGSize(width: 75.0, height: 75.0)
                let scaledImage = image?.af.imageScaled(to: size)
                let roundedImage = scaledImage?.af.imageRounded(withCornerRadius: 10.0)
                DispatchQueue.main.async {
                    self.userImg.image = roundedImage
                    self.userDetailsTableView.reloadData()
                }
            }
        }
    }

}


// MARK: Text field delegate
extension UserDetailsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// MARK: Work with image
extension UserDetailsViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func chooseImagePicker(source: UIImagePickerController.SourceType) {
        
        if UIImagePickerController.isSourceTypeAvailable(source) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            present(imagePicker, animated: true)
        }
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        userImg.image = info[.editedImage] as? UIImage
        userImg.contentMode = .scaleAspectFill
        userImg.clipsToBounds = true
        dismiss(animated: true)
    }
}

