//
//  SavedUsersTableViewCell.swift
//  UserDataTest
//
//  Created by Jhonny Green on 29.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import UIKit

class SavedUsersTableViewCell: UITableViewCell {

    @IBOutlet weak var savedUserImageView: UIImageView!
    @IBOutlet weak var savedUserNameLabel: UILabel!
    @IBOutlet weak var savedPhoneNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
