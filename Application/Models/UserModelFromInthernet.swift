//
//  UserModelFromInthernet.swift
//  UserDataTest
//
//  Created by Jhonny Green on 29.04.2020.
//  Copyright © 2020 Test Lessons. All rights reserved.
//

import Foundation

struct Empty: Decodable {
    let results: [Results]
    let info: Info
}

// MARK: - Info
struct Info: Decodable {
    let seed: String
    let results, page: Int
    let version: String
}

// MARK: - Results
struct Results: Decodable {
    let gender: Gender?
    var name: Name
    let location: Location?
    let email: String
    let login: Login?
    let dob, registered: Dob?
    let phone: String
    let cell: String?
    let id: ID?
    var picture: Picture
    let nat: String?
    
    
    
    init(entity: UserEntity, large: String, medium: String, thumbnail: String, title: String, first: String, last: String) {
        self.picture = Picture(large: large, medium: medium, thumbnail: thumbnail)
        self.name = Name(title: title, first: first, last: last)
        name.first = entity.firstName ?? ""
        name.last = entity.lastName ?? ""
        name.title = entity.title ?? ""
        picture.thumbnail = entity.image ?? ""
        self.email = entity.email ?? ""
        self.phone = entity.phone ?? ""
        self.gender = nil
        self.location = nil
        self.login = nil
        self.dob = nil
        self.registered = nil
        self.cell = nil
        self.id = nil
        self.nat = nil
    }
    
}



// MARK: - Dob
struct Dob: Decodable {
    let date: String
    let age: Int
}

enum Gender: String, Decodable {
    case female = "female"
    case male = "male"
}

// MARK: - ID
struct ID: Decodable {
    let name: String
    let value: String?
}

// MARK: - Location
struct Location: Decodable {
    let street: Street
    let city, state, country: String
    let coordinates: Coordinates
    let timezone: Timezone
}

// MARK: - Coordinates
struct Coordinates: Decodable {
    let latitude, longitude: String
}


// MARK: - Street
struct Street: Decodable {
    let number: Int
    let name: String
}

// MARK: - Timezone
struct Timezone: Decodable {
    let offset, timezoneDescription: String

    enum CodingKeys: String, CodingKey {
        case offset
        case timezoneDescription = "description"
    }
}

// MARK: - Login
struct Login: Decodable {
    let uuid, username, password, salt: String
    let md5, sha1, sha256: String
}

// MARK: - Name
struct Name: Decodable {
    var title, first, last: String
    init(title: String, first: String, last: String) {
        self.first = first
        self.last = last
        self.title = title
    }
    
    enum CodingKeys: String, CodingKey {
      case title
      case first
      case last
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decode(String.self, forKey: .title)
        first = try container.decode(String.self, forKey: .first)
        last = try container.decode(String.self, forKey: .last)
    }
}

// MARK: - Picture
struct Picture: Decodable {
    var large, medium, thumbnail: String
    
    init(large: String, medium: String, thumbnail: String) {
        self.large = large
        self.medium = medium
        self.thumbnail = thumbnail
    }
    
    enum CodingKeys: String, CodingKey {
      case large
      case medium
      case thumbnail
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        large = try container.decode(String.self, forKey: .large)
        medium = try container.decode(String.self, forKey: .medium)
        thumbnail = try container.decode(String.self, forKey: .thumbnail)
    }

}
